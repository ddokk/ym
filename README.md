
![](https://gitlab.com/ddokk/gfx/-/raw/main/g/b1.png)

----

<h1 align="center"><code> Concepts Illustration </code></h1>
<h1 align="center"><i>Based on this talk - https://twitter.com/YasminMogahed/status/1640067844565094407</i></h1>

----
1. [Repo Description](#repo-description)
2. [Diagram](#diagram)
   1. [Diagram Notes](#diagram-notes)

----

# Repo Description 

This repo is an illustration of the concepts which have been discussed in this vid 
[`https://twitter.com/YasminMogahed/status/1640067844565094407`](https://twitter.com/YasminMogahed/status/1640067844565094407)

![https://twitter.com/YasminMogahed/status/1640067844565094407](t/y.mp4)


# Diagram 

![](t/d.svg)

## Diagram Notes 

1. This diagram is has made using [`plantuml`](https://plantuml.com/) whose code can be found [`here`](./t/d.wsd)
2. This diagram is an `svg` rendered using [`https://planttext.com/`](https://planttext.com/)
3. The diagram is HD which can be downloaded for closer examination 

