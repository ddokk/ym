#!/bin/bash 

# Potato Update

# Update 
sudo apt-get update 
sudo apt-get dist-upgrade -Vy 
sudo apt-get autoremove -y
sudo apt-get autoclean
sudo apt-get clean
sudo apt install cmake -y
sudo apt install neofetch -y
rustup update
sudo apt-add-repository ppa:fish-shell/release-3 -y 
sudo apt-get install fish -y

# Cargo Installs 
cargo install exa fd-find ripgrep du-dust starship bat

# Helix and Fish 
brew install fzf helix 
yes | /home/linuxbrew/.linuxbrew/opt/fzf/install
fzf_key_bindings

# Starship
starship preset pure-preset > ~/.config/starship.toml
echo "###### Add shit to config fish ##########"
bash 
cat << EOF >> ~/.config/fish/config.fish
starship init fish | source

# --- This section is for fish --- #

# This is the exa command 
alias e="exa -labBghHimnSuU --git --icons --octal-permissions --icons --color-scale --color=always --group-directories-first"

# Command Aliases
alias e="exa -labBghH --git --octal-permissions --icons --color-scale --color=always --group-directories-first"
alias el="exa -labBghHimnSuU --git -@ --octal-permissions --icons --color-scale --color=always --group-directories-first"
alias fzf="fzf --preview 'bat --style=numbers --color=always --line-range :500 {}'"

# Github aliases 
alias gl="git log --oneline --decorate --graph"
alias gp="git add . && git commit && git push"
alias gb="git branch -a"
alias gsb="gb && git switch"
alias gpp="git add . && git commit --allow-empty --allow-empty-message && git push --progress"
EOF 